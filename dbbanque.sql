-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 17 sep. 2017 à 19:45
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `dbbanque`
--

DELIMITER $$
--
-- Procédures
--
DROP PROCEDURE IF EXISTS `ProcedureLogin`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ProcedureLogin` (IN `user_name` VARCHAR(10), IN `password` VARCHAR(50))  NO SQL
SELECT * from users
where login = @user_name
AND pass = @password$$

DROP PROCEDURE IF EXISTS `ProcedureLogin2`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ProcedureLogin2` (IN `user_name` VARCHAR(15) CHARSET utf8)  NO SQL
SELECT * FROM users
WHERE users.login = @user_name$$

DROP PROCEDURE IF EXISTS `VerifyUser`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `VerifyUser` (IN `user_name` VARCHAR(15), IN `motdepass` VARCHAR(50))  BEGIN
	SELECT * FROM users
    WHERE users.login = user_name AND users.pass = motdepass;
END$$

DROP PROCEDURE IF EXISTS `VerifyUser2`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `VerifyUser2` (IN `user_name` VARCHAR(15), IN `motdepass` VARCHAR(50), OUT `id2` INT(11) UNSIGNED, OUT `user_name2` VARCHAR(15), OUT `motdepass2` VARCHAR(50))  NO SQL
BEGIN
	SELECT id, login, pass INTO id2, user_name2, motdepass2 FROM users
    WHERE users.login = user_name AND users.pass = motdepass;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(15) NOT NULL,
  `type` varchar(10) NOT NULL,
  `amount` int(11) NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_client_fk` (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `accounts`
--

INSERT INTO `accounts` (`id`, `owner`, `type`, `amount`, `id_user`) VALUES
(3, 'bob', 'Compte', 1000, 1),
(4, 'bob', 'livret', 150, 1),
(5, 'Zak', 'CompteB', 1800, 2),
(6, 'larina', 'Livret', 10000, 3),
(7, 'Ziko', 'CompteB', 5000, 4);

-- --------------------------------------------------------

--
-- Structure de la table `connexion`
--

DROP TABLE IF EXISTS `connexion`;
CREATE TABLE IF NOT EXISTS `connexion` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(30) NOT NULL,
  PRIMARY KEY (`index`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `connexion`
--

INSERT INTO `connexion` (`index`, `ip`) VALUES
(1, '127.0.0.1'),
(2, '::1'),
(3, '::1'),
(4, '::1');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(10) NOT NULL,
  `pass` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `login`, `pass`) VALUES
(1, 'bob', '1234'),
(2, 'Zak', '123456'),
(3, 'Larina', '123456'),
(4, 'Ziko', 'anastasia');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
